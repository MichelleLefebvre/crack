#include <stdio.h>
#include "md5.h"
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    FILE *e = fopen(argv[1], "r");
    FILE *h = fopen(argv[2], "w");
    if(!e)
    {
        printf("Can't open %s for writing\n", argv[1]);
        exit(1);
    }
    if(!h)
    {
        printf("Can't open %s for writing\n", argv[2]);
        exit(1);
    }
    char pass[100];
    while (fscanf(e, " %s", pass)!= EOF)
    {
        char *hash = md5(pass, strlen(pass));
        fprintf(h, "%s\n", hash );
    }
    fclose(h);
    fclose(e);
}
